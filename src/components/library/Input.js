import styled from 'styled-components/native';
import {COLORS} from '../../constants';

const Input = styled.TextInput`
  border-radius: 10;
  background-color: ${COLORS.gray.four};
  color: ${({theme}) => theme.dittoWhite};
  font-size: 22;
  height: 55;
  padding-right: 15;
  padding-left: 15;
  min-width: 200;
  margin: ${({marginValue}) => (marginValue ? marginValue : '0')};
`;
export default Input;

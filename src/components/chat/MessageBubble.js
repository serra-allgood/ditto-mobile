import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import styled from 'styled-components/native';
import {MessageText, MessageImage, Time, utils} from 'react-native-gifted-chat';
import {isEmoji, getNameColor} from '../../utilities/Misc';
import {COLORS} from '../../constants';

const debug = require('debug')('ditto:component:MessageBubble');

export default class MessageBubble extends Component {
  renderMessageText() {
    if (this.props.currentMessage.text) {
      let reply = null;
      if (this.props.currentMessage.text.match(/>\s{1}.*\n\n/)) {
        const replyText = this.props.currentMessage.text.match(/>\s{1}.*\n\n/);
        debug('reply text', replyText);
        // debug('reply text', replyText[0]);
        reply = (
          <ReplyWrapper>
            <Reply>{replyText[0].trim().slice(1)}</Reply>
          </ReplyWrapper>
        );
        this.props.currentMessage.text = this.props.currentMessage.text.slice(
          replyText[0].length
        );
      }
      const {
        containerStyle,
        wrapperStyle,
        messageTextStyle,
        ...messageTextProps
      } = this.props;
      return (
        <>
          {reply}
          <MessageText
            {...messageTextProps}
            textStyle={{
              left: {color: COLORS.gray.one},
              right: {color: COLORS.gray.one}
            }}
          />
        </>
      );
    }
    return null;
  }

  renderMessageImage() {
    if (this.props.currentMessage.image) {
      const {containerStyle, wrapperStyle, ...messageImageProps} = this.props;
      return (
        <MessageImage
          {...messageImageProps}
          // imageStyle={[styles.slackImage, messageImageProps.imageStyle]}
        />
      );
    }
    return null;
  }
  render() {
    const {user} = this.props.currentMessage;
    const {previousMessage: prevMessage, nextMessage} = this.props;
    // debug('props', this.props);
    const showUsername = prevMessage.sender !== user._id;
    const isUser = this.props.user._id === user._id;
    const emojiMessage = isEmoji(this.props.currentMessage.text);

    const topCorner = prevMessage.sender === user._id;
    const bottomCorner = nextMessage.sender === user._id;
    return (
      <Wrapper>
        {showUsername && (
          <Username name={user.name || user.userId}>
            {!isUser ? user.name || user.user_id : 'Me'}
          </Username>
        )}
        {emojiMessage ? (
          <EmojiText>{this.props.currentMessage.text}</EmojiText>
        ) : (
          <BubbleWrapper
            isUser={isUser}
            topCorner={topCorner}
            bottomCorner={bottomCorner}>
            {this.renderMessageImage()}
            {this.renderMessageText()}
          </BubbleWrapper>
        )}
      </Wrapper>
    );
  }
}

const Wrapper = styled.View`
  flex-direction: column;
`;

const ReplyWrapper = styled.View`
  border-left-width: 3;
  border-left-color: ${COLORS.red};
  padding-top: 8;
  padding-bottom: 8;
  padding-left: 5;
  padding-right: 10;
  margin-left: 12;
  margin-top: 8;
  margin-bottom: 8;
`;

const Reply = styled.Text`
  color: rgba(255, 255, 255, 0.7);
  /* background-color: pink; */
`;

const BubbleWrapper = styled.View`
  background-color: ${({isUser}) =>
    isUser ? COLORS.dittoPurple : COLORS.headerColor};
  max-width: 300;
  border-radius: 16;
  padding-top: 5;
  padding-bottom: 5;
  padding-left: 2;
  padding-right: 2;
  ${({topCorner, isUser}) =>
    topCorner ? `border-top-${isUser ? 'right' : 'left'}-radius: 8;` : ''}
  ${({bottomCorner, isUser}) =>
    bottomCorner ? `border-bottom-${isUser ? 'right' : 'left'}-radius: 8;` : ''}
`;

const EmojiText = styled.Text`
  font-size: 50;
  margin-left: 10;
`;

const Username = styled.Text`
  color: ${({name}) => getNameColor(name)};
  margin-left: 12;
  margin-top: 6;
  margin-bottom: 3;
  font-weight: bold;
`;

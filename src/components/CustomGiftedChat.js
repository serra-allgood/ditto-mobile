import React, {Component} from 'react';
import {
  GiftedChat,
  Actions,
  Composer,
  Send,
  Bubble,
  InputToolbar
} from 'react-native-gifted-chat';
import styled from 'styled-components/native';
import {COLORS, SCREEN_WIDTH} from '../constants';
import {responsive} from '../utilities/ResponsiveValue';
import SendButton from '../assets/icons/icon-send.svg';
import MessageBubble from './chat/MessageBubble';
import { onUrlPress } from '../utilities/Misc';

const debug = require('debug')('ditto:component:CustomGiftedChat');

const CustomGiftedChat = props => (
  <GiftedChat
    {...props}
    style={{backgroundColor: 'red'}}
    isAnimated={true}
    // loadEarlier={true}
    // bottomOffset={responsive(103)}
    listViewProps={{
      ref: props.ref,
      onEndReached: props.onEndReached,
      keyboardDismissMode: 'on-drag'
    }}
    scrollToBottom={true}
    alwaysShowSend
    showAvatarForEveryMessage={true}
    // renderUsernameOnMessage={true}
    renderAvatar={() => null}
    renderBubble={renderBubble}
    renderComposer={renderComposer}
    // renderActions={renderActions}
    renderInputToolbar={renderInputToolbar}
    // renderMessage={renderMessage}
    parsePatterns={linkStyle => [
      {pattern: />\s{1}.*\n\n/, style: {color: COLORS.red}},
      {
        type: 'url',
        style: {
          color: COLORS.lightBlue,
          textDecorationLine: 'underline',
          textDecorationColor: COLORS.lightBlue,
          fontWeight: 'bold'
        },
        onPress: onUrlPress
      }
    ]}
  />
);
export default CustomGiftedChat;

// const renderMessage = ({currentMessage}) => (
//   <AnimatedBubble currentMessage={currentMessage} />
// );

//********************************************************************************
// Left Actions
//********************************************************************************

const Touchable = styled.TouchableOpacity`
  margin-left: 10;
  margin-right: 10;
  align-self: center;
`;

// const StyledIcon = styled(DittoIcon)`
//   color: ${COLORS.gray.one};
//   font-size: 20;
// `;

// const ActionButton = ({onPress, ...props}) => (
//   <Touchable onPress={onPress}>
//     <StyledIcon {...props} />
//   </Touchable>
// );

// const renderActions = () => <ActionButton name='camera' />;

//********************************************************************************
// Composer
//********************************************************************************

const composerStyle = {
  borderRadius: 15,
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 8,
  // paddingBottom: 5,
  color: COLORS.gray.one,
  backgroundColor: COLORS.gray.four,
  // height: 100,
  alignSelf: 'center'
};

//********************************************************************************
// Input Toolbar
//********************************************************************************

const SendWrapper = styled.View`
  margin-left: 10;
  margin-right: 10;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

const InputToolbarWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  width: ${SCREEN_WIDTH};
`;

const renderInputToolbar = props => (
  <InputToolbar
    {...props}
    containerStyle={{backgroundColor: COLORS.dark, borderTopColor: '#3e3358'}}
  />
);

// const renderInputToolbar = props => {
//   return (
//     <InputToolbarWrapper>
//       {/* {props.renderActions()} */}
//       <Composer textInputStyle={composerStyle} {...props} />
//       {/* <Send {...props}>
//         <SendWrapper>
//           <StyledIcon name='send' />
//           <SendButton />
//         </SendWrapper>
//       </Send> */}
//     </InputToolbarWrapper>
//   );
// };

const renderComposer = props => (
  <Composer
    {...props}
    textInputStyle={{
      // backgroundColor: '#0d091a',
      paddingTop: 10,
      color: '#fff'
    }}
  />
);

const renderBubble = props => {
  // debug('bubble props', props);
  // return (
  //   <Bubble
  //     {...props}
  //     textStyle={{left: {color: '#fff'}}}
  //     wrapperStyle={{
  //       left: {backgroundColor: COLORS.headerColor},
  //       right: {backgroundColor: COLORS.dittoPurple}
  //     }}
  //   />
  // );

  return <MessageBubble {...props} />;
};

const linkStyle = {
  color: 'red'
};

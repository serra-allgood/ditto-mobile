import React, {Component} from 'react';
import {StatusBar} from 'react-native';
import styled from 'styled-components/native';
// import DittoIcon from '../../assets/icons/DittoIcon';
import PageWrapper from '../../components/library/PageWrapper';
import Image from 'react-native-scalable-image';
import {SCREEN_WIDTH, COLORS} from '../../constants';
import Button from '../../components/library/Button';
import AuthFooter from '../../components/auth/AuthFooter';
import Text from '../../components/library/Text';
import WordmarkFile from '../../assets/icons/wordmark.svg';

const middleBlob = require('../../assets/images/blob1.png');
const topBlob = require('../../assets/images/blob2.png');
const bottomBlob = require('../../assets/images/blob3.png');

StatusBar.setBarStyle('light-content');

export default class LandingScreen extends Component {
  navToSignUp = () => this.props.navigation.navigate('SignUpOne');
  navToLogin = () => this.props.navigation.navigate('Login');

  render() {
    return (
      <PageWrapper>
        <TopBlob source={topBlob} />
        <MiddleBlob source={middleBlob} />
        <BottomBlob source={bottomBlob} />
        <Wordmark width={200} fill={COLORS.gray.one} />
        <TextWithMargin>Chat Collective</TextWithMargin>
        <ButtonWrapper>
          <Button disabled title='Sign Up' onPress={this.navToSignUp} />
        </ButtonWrapper>
        <AuthFooter mainText='Login' mainAction={this.navToLogin} />
      </PageWrapper>
    );
  }
}

const TopBlob = styled(Image)`
  position: absolute;
  top: -80;
  right: -100;
`;

const MiddleBlob = styled(Image)`
  position: absolute;
  top: -220;
  left: -420;
`;

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -340;
  left: -220;
`;

const Wordmark = styled(WordmarkFile)`
  margin-top: 100;
  margin-left: 30;
`;

const TextWithMargin = styled(Text)`
  margin-top: 12;
  margin-left: 40;
`;

const ButtonWrapper = styled.View`
  position: absolute;
  bottom: 180;
  justify-content: center;
  align-items: center;
  width: ${SCREEN_WIDTH};
  padding-top: 20;
  padding-bottom: 20;
`;

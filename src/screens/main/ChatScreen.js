import React, {Component} from 'react';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';
import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import {GiftedChat} from 'react-native-gifted-chat';
import {
  userDataSelector,
  clientSelector,
  currentMessagesSelector,
  currentRoomSelector
} from '../../redux/selectors';
import CustomGiftedChat from '../../components/CustomGiftedChat';
import {
  setCurrentMessages,
  formatMatrixEvent
} from '../../redux/actions/MatrixActions';
import {isEqual} from 'lodash';
import {
  fetchPreviousMessages,
  getRoomTimeline,
  retrieveHomeserverFromCache,
  retrieveDisplayNameListFromCache,
  saveDisplayNameListToCache,
  getAllRoomMembers
} from '../../api/matrix';

const debug = require('debug')('ditto:screens:ChatScreen');

class ChatScreen extends Component {
  static navigationOptions = ({navigation: nav}) => {
    const room = nav.getParam('room', 'Room');
    debug('room', room);
    return {
      title: room.name,
      headerTitleStyle: {
        color: COLORS.gray.one
      },
      headerLeft: <BackButton onPress={() => nav.goBack()} />,
      headerRight: null
    };
  };

  chatScrollView = null;

  state = {
    homeserver: '',
    messages: [],
    lastToken: '',
    displayNameList: []
  };

  formatForGiftedChat = () => {
    const {messages, userData} = this.props;
    const formatted = [];
    const { displayNameList } = this.state;

    messages.forEach(msg => {
      const msgValues = {
        _id: msg.event_id,
        createdAt: msg.origin_server_ts,
        user: {
          _id: msg.sender,
          name: displayNameList[msg.sender] || msg.sender
        }
      };
      if (msg.isTempMessage) {
        formatted.push(msg);
      } else if (msg.content?.msgtype === 'm.text') {
        const newMessage = {
          text: msg.content.body,
          ...msgValues,
          ...msg
        };
        formatted.push(newMessage);
      } else if (msg.type === 'm.room.name') {
        const room = this.props.navigation.getParam('room', null);
        room.name = msg.content.name;
        this.props.navigation.setParams({room});
        const newMessage = {
          text: `${msg.sender} changed the room name to ${msg.content.name}`,
          system: true,
          ...msgValues,
          ...msg
        };
        formatted.push(newMessage);
      } else if (msg.type === 'm.room.member') {
        if (msg.content.membership === 'join') {
          const newMessage = {
            text: `${msg.content.displayname} has joined`,
            system: true,
            ...msgValues,
            ...msg
          };
          formatted.push(newMessage);
        }
      } else if (msg.content?.msgtype === 'm.image') {
        const mxc = msg.content.url;
        const pieces = mxc.split('/');
        const imageUrl = `${this.state.homeserver}/_matrix/media/r0/download/${pieces[2]}/${pieces[3]}`;
        debug('imageUrl: ', imageUrl);
        const newMessage = {
          text: 'My message',
          image: imageUrl,
          ...msgValues,
          ...msg
        };
        formatted.push(newMessage);
      }
    });
    formatted.sort((a, b) => b.createdAt - a.createdAt);
    this.setState({messages: formatted});
    return formatted;
  };

  onSend = newMessages => {
    debug('new messages', newMessages);
    debug('my user id: ', this.props.userData.userId);

    const room = this.props.navigation.getParam('room', null);

    var content = {
      body: newMessages[0].text,
      msgtype: 'm.text'
    };

    const _id = Math.random() * 10000;
    const newMsg = {
      ...content,
      isTempMessage: true,
      _id,
      event_id: _id,
      sender: this.props.userData.userId,
      text: newMessages[0].text,
      createdAt: new Date(),
      user: {
        _id: this.props.userData.userId,
        name: this.props.userData.displayName || this.props.userData.userId
      }
    };

    this.props.setCurrentMessages([...this.props.messages, newMsg]);

    const currentRoom = this.props.navigation.getParam('room', null);
    this.props.client
      .sendEvent(currentRoom.roomId, 'm.room.message', content, '')
      .then(res => {
        debug('Message sent successfully', res);
      })
      .catch(err => {
        debug('Message could not be send: ', err);
      });
  };

  loadEarlierMessages = async () => {
    // debug('LOAD EARLIER MESSAGES');
    try {
      const room = this.props.navigation.getParam('room', null);
      const response = await fetchPreviousMessages(
        room.roomId,
        this.state.lastToken
      );
      debug('response', response)
      this.setState({lastToken: response.data.end});
      const prevMessages = response.data.chunk.map(msg =>
        formatMatrixEvent(msg)
      );
      debug('prevMessages', prevMessages);
      this.props.setCurrentMessages([...prevMessages, ...this.props.messages]);
      this.chatScrollView.scrollToEnd();
    } catch (error) {
      debug('Error fetching previous messages: ', error);
    }
  };

  updateMemberNamesInCache = async (members) => {
    const nameList = await retrieveDisplayNameListFromCache();
    members.forEach(member => {
      if (!nameList[member.user_id]) nameList[member.user_id] = member.content.displayname;
    });
    saveDisplayNameListToCache(nameList);
    this.setState({displayNameList: nameList});
  };

  //********************************************************************************
  // Lifecycle
  //********************************************************************************

  componentDidMount = async () => {
    const room = this.props.navigation.getParam('room', null);

    const members = await getAllRoomMembers(room.roomId);
    this.updateMemberNamesInCache(members);

    // const messages = room.getLiveTimeline().getEvents();
    const messages = getRoomTimeline(room);
    debug('messages', messages);
    this.props.setCurrentMessages(messages);

    const homeserver = await retrieveHomeserverFromCache();
    this.setState({homeserver}, () => {
      this.setState({messages: this.formatForGiftedChat()});
    });

    const response = await fetchPreviousMessages(room.roomId);
    // debug('last tokENNNN', response);
    this.setState({lastToken: response.data.end});
  };

  componentDidUpdate = ({messages}) => {
    if (!isEqual(messages, this.props.messages)) {
      this.setState({messages: this.formatForGiftedChat()});
    }
  };

  componentWillUnmount = () => {
    this.props.setCurrentMessages([]);
  };

  render() {
    // debug('this.props.messages', this.props.messages);
    return (
      <Wrapper>
        <CustomGiftedChat
          ref={ref => this.chatScrollView = ref}
          messages={this.state.messages}
          onSend={this.onSend}
          user={{
            _id: this.props.userData.userId,
            name: this.props.userData.displayName || this.props.userData.userId
          }}
          onEndReached={this.loadEarlierMessages}
        />
      </Wrapper>
    );
  }
}
const mapStateToProps = state => ({
  userData: userDataSelector(state),
  client: clientSelector(state),
  messages: currentMessagesSelector(state),
});
const mapDispatchToProps = {
  setCurrentMessages
};
export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);

const BackTouchable = styled.TouchableOpacity``;

const BackButton = ({onPress}) => (
  <BackTouchable onPress={onPress}>
    <Icon
      name='chevron-left'
      size={35}
      color={COLORS.gray.one}
      style={{marginLeft: 4}}
    />
  </BackTouchable>
);

const Wrapper = styled.View`
  flex: 1;
  background-color: ${COLORS.dark};
`;
